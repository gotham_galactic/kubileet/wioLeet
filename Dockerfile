FROM python:onbuild
ENV PORT 5000
EXPOSE 5000
RUN pip install .
ENTRYPOINT ["wioleet"]
CMD  ["serve"]
