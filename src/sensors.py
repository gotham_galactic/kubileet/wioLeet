sensors = {
    'analog': {
        'name': 'GenericAInA0',
        'params': {
            'analog': 'analog',
            'volt': 'voltage',
        }
    },
    'tempHumd': {
        'name': 'GroveTempHumD0',
        'params': {
            'humidity': 'humidity',
            'temperature': 'celsius_degree',
            'temperature_f': 'fahrenheit_degree',
        }
    }
}